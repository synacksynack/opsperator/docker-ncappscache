FROM docker.io/python:3-slim-buster

# NextCloud Applications Cache for OpenShift Origin

ARG DO_UPGRADE=
ENV CACHE_DIRECTORY=/cache/ \
    DEBIAN_FRONTEND=noninteractive \
    UVICORN_HOST=0.0.0.0 \
    UVICORN_PORT=8080

LABEL io.k8s.description="NextCloud Applications Cache Image." \
      io.k8s.display-name="NextCloud Applications Cache" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="nextcloud,applications,cache" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-ncappscache" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="1.0.0"

COPY config/*.py config/*.txt /app/

RUN set -x \
    && apt-get update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install NextCloud Applications Cache dependencies" \
    && apt-get install -y gcc make g++ curl \
    && python3 -m pip install -r /app/requirements.txt \
    && mkdir -p /cache \
    && echo "# Fixing permissions" \
    && chown -R 1001:0 /cache \
    && chmod 775 /cache \
    && echo "# Cleaning up" \
    && apt-get -y remove --purge gcc make g++ \
    && apt-get autoremove -y --purge \
    && rm -rvf /usr/share/man /usr/share/doc /var/lib/apt/lists/* \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

WORKDIR /app
EXPOSE $UVICORN_PORT
USER 1001
ENTRYPOINT uvicorn app:api --host ${UVICORN_HOST} --port ${UVICORN_PORT}
