# k8s NextCloud Applications Cache

Forked from https://git.r3ktm8.de/SeaLife-Docker/nextcloud-cache

Build with:

```
$ make build
```

Test with:

```
$ make demo
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name        |    Description                 | Default                  |
| :---------------------- | ------------------------------ | ------------------------ |
|  `CACHE_DIRECTORY`      | Applications Cache Directory   | `/cache`                 |
|  `UVICORN_PORT`         | NextCloud Apps Cache Bind Port | `8080`                   |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point | Description                            |
| :------------------ | -------------------------------------- |
|  `/cache`           | NextCloud Applications Cache Directory |
