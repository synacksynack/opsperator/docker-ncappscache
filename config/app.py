import os
from fastapi import FastAPI
from dotenv import load_dotenv
from prometheus_client.registry import REGISTRY
from starlette_prometheus import PrometheusMiddleware, metrics
from nextcloud_exporter import NextcloudExporter
from nextcloud_cache import cache

load_dotenv('.env.local')

api = FastAPI()


@api.get('/', include_in_schema=False)
def root():
    return {"message": "NextCloud App-Store Cache"}


REGISTRY.register(NextcloudExporter())

api.add_middleware(PrometheusMiddleware)
api.add_route('/metrics/', metrics)

api.include_router(cache)
