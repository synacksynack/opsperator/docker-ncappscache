import os

import json
import requests
from fastapi import APIRouter

cache = APIRouter()

cache_directory = os.getenv('CACHE_DIRECTORY', './cache/')

if not os.path.exists(cache_directory):
    os.mkdir(cache_directory)


def get_cached_file(file_name: str):
    if os.path.exists(cache_directory + file_name):
        with open(cache_directory + file_name, 'r') as fh:
            data = fh.read()

            return json.loads(data)

    else:
        try:
            response_from_service = requests.get('https://apps.nextcloud.com/api/v1/' + file_name, timeout=30)

            if 300 > response_from_service.status_code >= 200:
                with open(cache_directory + file_name, 'wb') as fh:
                    fh.write(response_from_service.content)
            else:
                return []

            return response_from_service.json()
        except requests.ConnectionError:
            return []
        except requests.exceptions.ReadTimeout:
            return []


@cache.get('/api/v1/categories.json')
def get_categories():
    return get_cached_file('categories.json')


@cache.get('/api/v1/apps.json')
def get_apps():
    return get_cached_file('apps.json')


@cache.get('/api/v1/platforms.json')
def get_platforms():
    return get_cached_file('platforms.json')
